<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\helpers\Markdown;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Post */

// Get user roles
$isAuthor = Yii::$app->user->can('author');

$this->title = $model->recordName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$this->context->leftMenu = [
    [
        'label' => Yii::t('app', 'Operations'),
        'items' => [
            [ 'label' => Yii::t('app', 'Update'), 'url' => ['update', 'id'=>$model->id]],
        ],
    ],
];

?>
<div class="post-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo Nav::widget([
        'items' => [
            [ 'label' => '<span class="glyphicon glyphicon-pencil"></span>'.' '.Yii::t('app', 'Update'), 'url' => ['update', 'id'=>$model->id]],
        ],
        'encodeLabels' => false,
        'options' => ['class' => 'nav-pills alert alert-info action-buttons'],
    ]); ?>
    <div style="font-weight:bold;font-size:larger">
    <?= str_replace('<table>', '<table class="table table-striped">', Markdown::process(str_replace([".\r\n", "!\r\n"], [".  \r\n", "!  \r\n"], $model->intro), 'extra')) ?>
    </div>

    <p><?= str_replace('<table>', '<table class="table table-striped">', Markdown::process(str_replace([".\r\n", "!\r\n"], [".  \r\n", "!  \r\n"], $model->content), 'extra')) ?></p>

    <hr>

    <h3><?= Yii::t('app','Other Infos') ?></h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'status',
                'value' => $model->statusOptions[$model->status],
            ],
            'id',
        ],
    ]) ?>

    <!-- {{{ History -->
    <h3><?= Yii::t('app', 'History') ?></h3>
    <?= DetailView::widget([
       'model' => $model,
       'attributes' => [
           ['attribute'=>'created_at', 'format'=>'html', 'value'=>Yii::$app->formatter->asDateTime($model->created_at)],
           ['attribute'=>'createUserName', 'format'=>'html'],
           ['attribute'=>'updated_at', 'format'=>'html', 'value'=>Yii::$app->formatter->asDateTime($model->updated_at)],
           ['attribute'=>'updateUserName', 'format'=>'html'],
       ],
    ]) ?>
    <!-- }}} -->

    <?php if($isAuthor) : /* {{{ DELETE BTN */ ?>     <p>
        <?= Html::a('<span class="glyphicon glyphicon-trash"></span>&nbsp;'.Yii::t('app', 'Delete this record'), ['delete', 'id' => $model->id], [
            'title' => Yii::t('app','Click to confirm deleting post: {recordName}', ['recordName'=>$model->recordName]),
            'class' => 'btn btn-danger btn-xs hidden-print',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?php endif; /* }}} */ ?>

</div>
