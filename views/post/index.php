<?php

use yii\bootstrap\Nav;
use yii\helpers\Html;
use yii\web\View;
use \app\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

// Get user roles
$isAuthor = Yii::$app->user->can('author');

$this->title = Yii::t('app', 'Posts');
$this->params['breadcrumbs'][] = $this->title;
/* $this->context->leftMenu = [ // {{{ 
    [
        'label' => Yii::t('app', 'Operations'),
        'items' => [
            [ 'label' => Yii::t('app', 'New Post'), 'url' => ['create'], 'icon'=>'plus'],
            [ 'label' => Yii::t('app', 'Clear Filters'), 'url' => '#', 'bodyOptions'=>['id'=>'clearFiltersBtn'], 'icon'=>'refresh' ],
        ],
    ],
]; */ // }}} 
?>
<div class="post-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo Nav::widget([
        'items' => [
            [ 'label' => '<span class="glyphicon glyphicon-plus"></span>'.' '.Yii::t('app', 'New Post'), 'url' => ['create'], 'visible'=>$isAuthor],
            [ 'label' => '<span class="glyphicon glyphicon-refresh"></span>'.' '.Yii::t('app', 'Clear Filters'), 'url' => '#', 'linkOptions'=>['id'=>'clearFiltersBtn'] ],
            [ 'label' => '<span class="glyphicon glyphicon-filter"></span>'.' '.Yii::t('app', 'Save Filter Set'), 'url' => ['launch/save-filters', 'gridId'=>'', 'searchModel'=>'LaunchSearch'], 'linkOptions'=>['id'=>'saveFiltersBtn'] ],
            [ 'label' => '<span class="glyphicon glyphicon-th-list"></span>'.' '.Yii::t('app', 'All Saved Filter Sets'), 'url' => ['listfilter/index', 'ListfilterSearch[route]'=>'post/index'], 'linkOptions'=>['id'=>'listFiltersBtn'] ],
        ],
        'encodeLabels' => false,
        'options' => ['class' => 'nav-pills alert alert-info action-buttons'],
    ]);
    ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php // {{{ Clear Filters Form ?>
    <?php  $form = Html::beginForm(['index'], 'post', ['id'=>'clear-filters-form', 'style'=>'display:inline']); ?>
    <?php  $form .= Html::hiddenInput('clear-state', '1'); ?>
    <?php  $form .= Html::hiddenInput('redirect-to', ''); ?>
    <?php  $form .= Html::endForm(); ?>
    <?php  echo $form; /* }}} */ ?>

    <?= GridView::widget([ // {{{ 
        'dataProvider' => $dataProvider,
        'as filterBehavior' => \thrieu\grid\FilterStateBehavior::className(),
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}',
                'contentOptions' => [ 'class' => 'text-center' ],
            ],
            'id',
            [
                'attribute' => 'title',
                'format'    => 'html',
                'value'     => function ($data) {
                    return Html::tag('b', Html::a($data->title, ['view', 'id'=>$data->id], ['title' => Yii::t('app', 'View Post: {recordName}', ['recordName'=>$data->recordName])]));
                },
            ],
            [
                'attribute' => 'status',
                'filter' => \app\models\Post::getStatusOptions(),
                'value' => function($data) {
                    return \app\models\Post::getStatusOptions()[$data->status];
                }
            ],
            'intro:ntext',
            'content:ntext',
            [
                'attribute' => 'created_at',
                'format' => 'datetime',
            ],
            //'created_by',
            //'updated_at',
            //'updated_by',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{delete}',
                'contentOptions' => [ 'class' => 'text-center' ],
            ],
        ],
    ]); /* }}} */?>
</div>

<?php $this->registerJs("
$('#clearFiltersBtn').on('click', function() { 
    $('#clear-filters-form').submit();
});
", View::POS_READY, 'my-button-handler' ); ?>
