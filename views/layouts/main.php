<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

// Create copyright text
$copyrightFirstYear = Yii::$app->params['applicationFirstYear'];
$cTxt = $copyrightFirstYear;
$thisYear = date('Y');
if($thisYear>$copyrightFirstYear)
    $cTxt .= ' - '.$thisYear;

// Select default bootswatch theme, as per app paramms
$theme = Yii::$app->params['theme'];

$cookiesRequest     = Yii::$app->request->cookies;
$cookiesResponse    = Yii::$app->response->cookies;
$themeNew = Yii::$app->request->get('theme');
if($themeNew) {
    // {{{ Change theme
    if(raoul2000\bootswatch\BootswatchAsset::isSupportedTheme($themeNew))
    // if(isset($this->languages[$languageNew]))
    {
        $theme = $themeNew;
        $cookiesResponse->add(new \yii\web\Cookie([
            'name' => 'theme',
            'value' => $themeNew
        ]));
    }
    // }}} 
} elseif($cookiesRequest->has('theme')) {
    $theme = $cookiesRequest->getValue('theme');
}

// Get array of themes
$availableThemeDirs = [ // {{{ 
    'cerulean',
    'cosmo',
    'cyborg',
    'darkly',
    'flatly',
    'journal',
    'lumen',
    'paper',
    'readable',
    'sandstone',
    'simplex',
    'slate',
    'spacelab',
    'superhero',
    'yeti',
];
ksort($availableThemeDirs);
// }}} 

$logoWhitePostfix = '';
// These themes are 'dark' and require a white Opel logo:
$themesLogoWhite = [ // {{{
    'cerulean', 
    'cosmo',
    'cyborg',
    'darkly',
    'flatly',
    'sandstone',
    'slate',
    'superhero',
    'yeti',
]; // }}} 
// Is current theme a 'dark' one, requiring a white logo?
if(in_array($theme, $themesLogoWhite))
    $logoWhitePostfix = '_weiss';

$availableThemeItems = array();
// print_r($availableThemeDirs);
foreach($availableThemeDirs as $themeDir)
{
    $temp = [];
    $temp['active'] = $theme == $themeDir ? true : false;
    $temp['label'] = ucfirst($themeDir);
    $temp['url'] = Url::current(['theme' => $themeDir]);
    $availableThemeItems[] = $temp;
}
raoul2000\bootswatch\BootswatchAsset::$theme = $theme;
AppAsset::register($this);

// Check if app runs on localhost
$titlePrefix = '';
$breadcrumbsCssClass = ['breadcrumb'];
if(strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
    $titlePrefix = 'LOCALHOST ';
    $breadcrumbsCssClass = array_merge(['bg-warning'], $breadcrumbsCssClass);
}
$this->title .= ' :: '.Yii::$app->name;

// {{{ *** SETUP MENU ITEMS ***
$isGuest = Yii::$app->user->isGuest;
$isAdmin = !Yii::$app->user->isGuest && Yii::$app->user->identity->isAdmin;
$isDev = !Yii::$app->user->isGuest && strtolower(Yii::$app->user->identity->username)==='xzndzw';
$menuItems = [];
// {{{ HOME
$menuItems[] = ['label' => '<span class="glyphicon glyphicon-home"></span> '.Yii::t('app', 'Home'), 'url' => ['/site/index']]; // }}} 
if ($isGuest) {
    // {{{ Login
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-plus"></span> '.Yii::t('app', 'Sign Up'), 'url' => ['/user/registration/register'], 'options'=>['id'=>'mnuRegister']]; // }}} 
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-in"></span> '.Yii::t('app', 'Sign In'), 'url' => ['/user/security/login'], 'options'=>['id'=>'mnuSignIn']]; // }}} 
} else {
    // {{{ APP PAGES
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-cog"></span> ' .\Yii::t('app','App'), 'items' => [
        ['label' => '<span class="glyphicon glyphicon-road"></span> '           .\Yii::t('app','Blog'),         'url' => ['/post/index']],
        ['label' => '<span class="glyphicon glyphicon-euro"></span> '           .\Yii::t('app','Action 2'),   'url' => ['/controller/action-2']],
    ]]; // }}} 
    // {{{ SETTINGS
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-wrench"></span> '.Yii::t('app','Settings'), 'items' => [
        ['label'=>'<span class="glyphicon glyphicon-wrench"></span> '.Yii::t('app','Site Settings'), 'url'=>['/site/manage-settings'], 'visible'=>!$isGuest && $isAdmin],
        ['label' => '<span class="glyphicon glyphicon-user"></span> '.Yii::t('app','Profile Settings (User)'), 'url' => ['/user/settings']],
    ]]; // }}} 
    $idx = count($menuItems)-1;
    if($isAdmin) {
        $oldItems = $menuItems[$idx]['items'];
        // {{{ ADMIN
        $newItems = [
            '<li class="divider"></li>',
            ['label' => '<span class="glyphicon glyphicon-list-alt"></span> '       .\Yii::t('app','Posts Admin'),      'url' => ['/post/index'], 'visible'=>!$isGuest && $isAdmin],
            ['label' => '<span class="glyphicon glyphicon-user"></span> '           .\Yii::t('app','Users Admin'),      'url' => ['/user/admin', 'sort'=> '-last_login_at'], 'visible'=>!$isGuest && $isAdmin],
            ['label' => '<span class="glyphicon glyphicon-user"></span> '           .\Yii::t('app','Roles and Users'),  'url' => ['/user-management/roles-and-users']],
            ['label' => '<span class="glyphicon glyphicon-search"></span> '         .\Yii::t('app','Lookups'),          'url' => ['/lookup'], 'visible'=>$isAdmin],
            ['label' => '<span class="glyphicon glyphicon-floppy-save"></span> '    .\Yii::t('app','DB Backup'),        'url' => ['/backuprestore'], 'visible'=>!$isGuest && $isAdmin],
        ]; // }}} 
        // {{{ DEVELOPER
        if($isDev) {
            $newItems[] = '<li class="divider"></li>';
            $newItems[] = ['label' => '<span class="glyphicon glyphicon-cog"></span> '.'Gii', 'url' => ['/gii']];
        } // }}} 
        $menuItems[$idx]['items'] = array_merge($oldItems, $newItems);
        /* {{{ DEBUG
        \yii\helpers\VarDumper::dump($oldItems, 10, true);
        \yii\helpers\VarDumper::dump($newItems, 10, true);
        echo '<hr>';
        \yii\helpers\VarDumper::dump($menuItems, 10, true);
        die;
        }}} */
    }
    // {{{ THEMES
    $menuItems[$idx]['items'][] = '<li class="divider"></li>';
    $menuItems[$idx]['items'][] = ['label' => '<span class="glyphicon glyphicon-tint"></span> '.\Yii::t('app','Themes ({theme})', ['theme'=>ucfirst($theme)]), 'items' => $availableThemeItems];
    // }}} 
    // {{{ LOGOUT
    $menuItems[] = ['label' => '<span class="glyphicon glyphicon-log-out"></span> '.Yii::t('app', 'Logout ({username})', ['username'=>Yii::$app->user->identity->username]), 'url' => ['/user/security/logout'], 'linkOptions' => ['data-method' => 'post']]; // }}}
    // {{{ SWITCH USER
    if(Yii::$app->session->has(\dektrium\user\controllers\AdminController::ORIGINAL_USER_SESSION_KEY)) {
        $menuItems[] = '<li>' . Html::beginForm(['/user/admin/switch'], 'post', ['class' => 'navbar-form'])
        . Html::submitButton('<span class="glyphicon glyphicon-user"></span> Back to original user',
            ['class' => 'btn btn-link']
        ) . Html::endForm() . '</li>';
    } // }}} 
}
// {{{ LANGUAGES
$menuItems[] = ['label' => Yii::t('app', 'Language'), 'items' => [
    ['label' => Yii::t('app', 'English'), 'url' => Url::current(['language'=>'en'])],
    ['label' => Yii::t('app', 'German'), 'url' => Url::current(['language'=>'de'])],
]]; // }}} 
// }}} End Menu Items

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?= Url::to('@web/favicon.ico', true) ?>" rel="shortcut icon" type="image/x-icon">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= $titlePrefix ?><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    $navbarInverse = '';
    if(!empty($_GET['nbi']))
        $navbarInverse = ' navbar-inverse';
    NavBar::begin([ // {{{ 
        'brandLabel' => Html::img('@web/img/Logo'.$logoWhitePostfix.'.png').'&nbsp;&nbsp;'.Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        // Make nav bar extend to ful width:
        'innerContainerOptions'  => [
            'class' => 'container-fluid',
        ],
        'options' => [
            'class' => 'navbar-default navbar-fixed-top'.$navbarInverse,
        ],
    ]);
    echo Nav::widget([
        'encodeLabels' => false,
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end(); // }}} 
    ?>

    <?php 
    // controller/$fluid requests where to show fluid layout
    $divClass='container';
    if(isset($this->context->fluid) and $this->context->fluid==true)
        $divClass .= '-fluid';
    ?>

    <div class="<?= $divClass ?>">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            'options' => ['class' => join(' ', $breadcrumbsCssClass)]
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="<?= $divClass ?>">
        <p class="pull-left"><b><?= Yii::$app->name ?></b> | 
            By <?= Html::a("Diggin' Data", 'https://www.diggin-data.de', ['target'=>'_blank']) ?> <?= $cTxt ?> | 
            <?= Html::a(Yii::t('app', 'Contact'), ['/site/contact']) ?> 
            
        </p>

        <p class="small pull-right">Rev. <?= Yii::$app->version ?>&nbsp;|&nbsp;<?= Html::a(Yii::t('app', 'Rev. History'), ['/site/page', 'view'=>'changes']) ?>&nbsp;|&nbsp;<?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
