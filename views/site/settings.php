<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $model \app\models\forms\ConfigurationForm */
/* @var $this \yii\web\View */

$this->title = Yii::t('app', 'Manage Application Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Settings')];
?>

<div class="settings-form">

    <h1><?= Html::encode('Manage Application Settings') ?></h1>

    <p class="well">
        <?= Yii::t('app','Sections:') ?>
        <a href="#emailAddresses"><?= Yii::t('app', 'Email Addresses') ?></a> | 
        <a href="#homePage"><?= Yii::t('app', 'Home Page') ?></a> | 
        <a href="#sendLaunchEmail"><?= Yii::t('app', 'Send Launch Email') ?></a> | 
        <a href="#actionItems"><?= Yii::t('app', 'Action Items') ?></a> | 
        <a href="#markets"><?= Yii::t('app', 'Markets') ?></a> | 
        <a href="#otherSettings"><?= Yii::t('app', 'Other Settings') ?></a> 

    <?php $form = ActiveForm::begin(); ?>

    <fieldset id="emailAddresses">
    <legend><?= Yii::t('app','Email Addresses') ?></legend>
    <p><?= Yii::t('app', 'These email address settings are used to send notification emails.') ?></p>
    <?php echo $form->field($model, 'contactEmail')->hint('Email address which receives contact form enquiries'); ?>
    <?php echo $form->field($model, 'adminEmail'); ?>
    </fieldset>

    <fieldset id="homePage">
    <legend><?= Yii::t('app','Home Page') ?></legend>
    <?php echo $form->field($model, 'homeNewsItemsPerPage')->textInput()->hint('How many news items shall be displayed on the Home page?'); ?>
    <?php echo $form->field($model, 'homeNewsItemWordLimit')->textInput()->hint('How many words per news item shall be displayed on the Home page?'); ?>
    </fieldset>

    <fieldset id="sendLaunchEmail">
    <legend><?= Yii::t('app','Send Launch Email') ?></legend>
    <p><?= Yii::t('app', 'These settings are used as default when sending a lauch notification email.') ?></p>
    <?php echo $form->field($model, 'launchSendEmailSubject')->textInput()->hint(Yii::t('app','Message subject. The placeholders {{LAUNCHNAME} and {{APPNAME}} get replaced.')); ?>
    <?php echo $form->field($model, 'launchSendEmailBcc')->hint('Email address to be set on Bcc.'); ?>
    <?php echo $form->field($model, 'launchSendEmailMessage')->textarea(['columns'=>40, 'rows'=>4])->hint(Yii::t('app','Message text. The placeholders {{LINK} and {{APPNAME}} get replaced.')); ?>
    </fieldset>

    <fieldset id="actionItems">
    <legend><?= Yii::t('app','Action Items') ?></legend>
    <?php echo $form->field($model, 'actionItemMarketCommentsMaxChars')->textInput()->hint(Yii::t('app','How many characters are allowed in comments?')) ?>
    <?php echo $form->field($model, 'actionItemMarketTokenExpirationTime')->textInput()->hint(Yii::t('app','How many hours shall a token to update an action item be valid?')) ?>
    <?php echo $form->field($model, 'actionItemMarketTokenUpdateTime')->textInput()->hint(Yii::t('app','How many hours shall a token to update an action item again be valid?')) ?>
    </fieldset>

    <fieldset id="markets">
    <legend><?= Yii::t('app','Top X Markets') ?></legend>
    <?php echo $form->field($model, 'topXMarkets')->textarea(['columns'=>40, 'rows'=>3])->hint(Yii::t('app','Define Top X Market groups. Enter a label and 2-char. country codes, separated by >. Separate Top X groups by |.').'<br>'.Yii::t('app', 'Example: <em>Top 5>DE;ES;FR;IT;UK|Top 9>AT;FR;DE;IT;PL;PT;ES;CH;UK</em>')) ?>
    <?php echo $form->field($model, 'reportResponsesByMarketShowTopX')->textInput()->hint(Yii::t('app',"Which Top X Market group to show in report 'Responses by Market'.") . ' ' . Yii::t('app', "Must exist as a key in setting 'Top X Markets").'<br>'.Yii::t('app', 'Example: <em>Top 9</em>')) ?>
    </fieldset>

    <fieldset id="otherSettings">
    <legend><?= Yii::t('app','Other Settings') ?></legend>
    <?php echo $form->field($model, 'launchLegend')->textarea(['columns'=>40, 'rows'=>8]) ?>
    </fieldset>

    <?= Html::submitButton('<span class="glyphicon glyphicon-ok"></span> ' . Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    <?= Html::resetButton('<span class="glyphicon glyphicon-refresh"></span> ' . Yii::t('app', 'Reset'), ['class' => 'btn btn-primary']) ?>

    <?php ActiveForm::end(); ?>

</div>
