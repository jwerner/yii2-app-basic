<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;

use app\models\LoginForm;
use app\models\ContactForm;
use app\components\BaseController;
use app\components\General;

class SiteController extends BaseController
{
    // {{{ behaviors
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'allow' => false,
                        'actions' => ['rules'],
                    ],
  [
                        'actions' => ['rules', 'logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    } // }}} 
    // {{{ actions
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'manage-settings' => [
                'class' => \yii2mod\settings\actions\SettingsAction::class,
                // also you can use events as follows:
                'on beforeSave' => function ($event) {
                    // your custom code
                },
                'on afterSave' => function ($event) {
                    // your custom code
                },
                'modelClass' => \app\models\forms\AppParamsForm::class,

                'view' => 'settings',
                'successMessage' => '<h4>'.Yii::t('app','Saving Settings').'</h4>'.Yii::t('app','The settings have been saved successfully.'),


            ],
            'page' => [
                'class' => 'yii\web\ViewAction',
            ],
        ];
    } // }}} 
    // {{{ actionIndex
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $this->fluid = true;
        $report = null;

        // if(!Yii::$app->user->isGuest)
        //    $report = Yii::$app->runAction('report/responses-by-markets', ['partial'=>true]);
        // return $this->render('index', ['report'=>$report]);
        return $this->render('index');
    } // }}} 
    // {{{ actionLogin
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    } // }}} 
    // {{{ actionLogout
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    } // }}} 
    // {{{ actionContact
    /** 
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->settings->get('AppParamsForm', 'contactEmail'))) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    } // }}} 
    // {{{ actionAbout
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    } // }}} 
    // {{{ actionRules
    /**
     * Displays the Rules page.
     *
     * @return string
     */
    public function actionRules()
    {
        return $this->render('rules');
    } // }}} 
    // {{{ actionSendMail
    /**
     * Sends a mail using mailerqueue.
     *
     * @return string
     */
    public function actionSendMail()
    {
        for($i=0; $i<1; $i++) {
            Yii::$app->mailqueue->compose('send-mail', ['data' => ['description'=>'Mail #'.($i+1)]])
                ->setFrom('joachim.pt.werner@opel.com')
                ->setTo('joachim.pt.werner@opel.com')
                ->setSubject('Test mailqueue #'.($i+1))
                ->setTextBody('Bla Blub...')
                ->queue();
        }
        return $this->render('about');
    } // }}} 
    // {{{ actionHelp
    public function actionHelp($page='README')
    {
        // $this->fluid=true;
        if(!file_exists(Yii::getAlias('@app/views/site/markdown/'.$page.'.md')))
            throw new NotFoundHttpException(Yii::t('app', 'The requested help page does not exist.'));
        $markdown = file_get_contents(Yii::getAlias('@app/views/site/markdown/'.$page.'.md'));
        return $this->render('help', ['page'=>$page, 'markdown'=>$markdown]);
    } // }}} 
    // {{{ actionRunInitialMigrations
    /**
     * Run initial database migrations
     */
    public function actionRunInitialMigrations() {
        $this->layout = false;
        echo '<h1>Running Database Migrations</h1>';
        echo '<pre>';
        echo Html::tag('h2', 'Create RBAC Tables');
        General::runConsoleActionFromWebApp('migrate', ['migrationPath' => '@yii/rbac/migrations', 'interactive' => false]);
        echo Html::tag('h2', 'Run yii2-user module migrations');
        General::runConsoleActionFromWebApp('migrate', ['migrationPath'=>'@vendor/dektrium/yii2-user/migrations', 'interactive' => false]);
        echo Html::tag('h2', 'Run yii2-settings module migrations');
        General::runConsoleActionFromWebApp('migrate', ['migrationPath'=>'@vendor/yii2mod/yii2-settings/migrations', 'interactive' => false]);
        echo Html::tag('h2', 'Run Application migrations');
        General::runConsoleActionFromWebApp('migrate', ['interactive' => false]);
        echo Html::tag('h2', 'Run lookup module migrations');
        General::runConsoleActionFromWebApp('migrate', ['migrationPath'=>'@app/modules/lookup/migrations', 'interactive' => false]);
        echo Html::tag('h2', 'Run Application Settings creation');
        General::runConsoleActionFromWebApp('init-app');
        echo Html::tag('h2', 'FINISHED MIGRATIONS :-)');
        echo '<pre>';
        die;
    } // }}}
}
