<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

use yii2mod\settings\models\SettingModel as Setting;

/**
 * This command initializes the application.
 *
 * @author Joachim Werner <joachim.werner@diggin-data.de>  
 */
class InitAppController extends Controller
{
    /**
     * This command initializes the app.
     *
     * - Initalize site settings
     *
     */
    public function actionIndex()
    {
        echo "\n";
        echo "init-app\n";
        echo "--------\n\n";

        $settings = \Yii::$app->settings;

        echo "Creating Settings entries...\n";

        // $settings->set('section', 'key', 125.5);
        // or: $settings->set('section', 'key', 'false', SettingType::BOOLEAN_TYPE);

        $section = 'AppParamsForm';

        // {{{ Email Settings

        $key            = 'contactEmail'; // {{{ 
        $value          = 'contact@email.tld';
        $description    = 'Email address which receives contact form enquiries';
        echo $section.' > '.$key.'...';
        $settings->set($section, $key, $value);
        $setting = Setting::find()->where(['section'=>$section, 'key'=>$key])->one();
        $setting->description = $description;
        $setting->save();
        echo "OK\n"; // }}} 

        $key            = 'adminEmail'; // {{{ 
        $value          = 'admin@email.tld';
        $description    = 'Email address which receives admin notifications';
        echo $section.' > '.$key.'...';
        $settings->set($section, $key, $value);
        $setting = Setting::find()->where(['section'=>$section, 'key'=>$key])->one();
        $setting->description = $description;
        $setting->save();
        echo "OK\n"; // }}} 

        // }}} 

        $key            = 'homeNewsItemsPerPage'; // {{{ 
        $value          = 3;
        $description    = 'How many news items shall be displayed on the Home page?';
        echo $section.' > '.$key.'...';
        $settings->set($section, $key, $value);
        $setting = Setting::find()->where(['section'=>$section, 'key'=>$key])->one();
        $setting->description = $description;
        $setting->save();
        echo "OK\n"; // }}} 

        $key            = 'homeNewsItemWordLimit'; // {{{ 
        $value          = 30;
        $description    = 'How many words per news item shall be displayed on the Home page?';
        echo $section.' > '.$key.'...';
        $settings->set($section, $key, $value);
        $setting = Setting::find()->where(['section'=>$section, 'key'=>$key])->one();
        $setting->description = $description;
        $setting->save();
        echo "OK\n"; // }}} 

        echo "\nFinished.\n";
    }
}
