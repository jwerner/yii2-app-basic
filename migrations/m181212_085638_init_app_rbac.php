<?php

use yii\db\Migration;

/**
 * Class m181212_085638_init_app_rbac
 */
class m181212_085638_init_app_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181212_085638_init_app_rbac cannot be reverted.\n";

        return false;
    }

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $auth = Yii::$app->authManager;

        $auth->removeAll();

        // {{{ add rules
        // add the Author rule
        $authorRule = new \app\rbac\AuthorRule;
        $auth->add($authorRule);

        // add the CountryManager rule
        // $countryManagerRule = new \app\rbac\CountryManagerRule;
        // $auth->add($countryManagerRule);
        // }}} 
        // Author
        $author = $auth->createRole('author');
        $author->description = 'Author';
        $auth->add($author);

        // Market Launch Coordinator
        $mlc = $auth->createRole('marketLaunchCoordinator');
        $mlc->description = 'Market Launch Coordinator';
        $auth->add($mlc);
        
        // {{{ Launch
        // add "manageLaunch" permission
        $manageLaunch = $auth->createPermission('manageLaunch');
        $manageLaunch->description = 'Manage launches';
        $auth->add($manageLaunch);

        // add "createLaunch" permission
        $createLaunch = $auth->createPermission('createLaunch');
        $createLaunch->description = 'Create a launch';
        $auth->add($createLaunch);

        // add "updateLaunch" permission
        $updateLaunch = $auth->createPermission('updateLaunch');
        $updateLaunch->description = 'Update launch';
        $auth->add($updateLaunch);

        // add "deleteLaunch" permission
        $deleteLaunch = $auth->createPermission('deleteLaunch');
        $deleteLaunch->description = 'Delete launch';
        $auth->add($deleteLaunch);
        // }}} 

        // {{{ Launch Market
        // add "manageLaunchMarket" permission
        $manageLaunchMarket = $auth->createPermission('manageLaunchMarket');
        $manageLaunchMarket->description = 'Manage launch Markets';
        $auth->add($manageLaunchMarket);

        // add "createLaunchMarket" permission
        $createLaunchMarket = $auth->createPermission('createLaunchMarket');
        $createLaunchMarket->description = 'Create a launch market';
        $auth->add($createLaunchMarket);

        // add "updateLaunchMarket" permission
        $updateLaunchMarket = $auth->createPermission('updateLaunchMarket');
        $updateLaunchMarket->description = 'Update launch market';
        $auth->add($updateLaunchMarket);

        // add the "updateOwnLaunchMarket" permission and associate the rule with it.
        // $updateOwnLaunchMarket = $auth->createPermission('updateOwnLaunchMarket');
        // $updateOwnLaunchMarket->description = 'Update own launch market';
        // $updateOwnLaunchMarket->ruleName = $countryManagerRule->name;
        // $auth->add($updateOwnLaunchMarket);

        // "updateOwnLaunchMarket" will be used from "updateLaunchMarket"
        // $auth->addChild($updateOwnLaunchMarket, $updateLaunchMarket);

        // allow "mlc" to update their own posts
        // $auth->addChild($mlc, $updateOwnLaunchMarket);        

        // add "deleteLaunch" permission
        $deleteLaunchMarket = $auth->createPermission('deleteLaunchMarket');
        $deleteLaunchMarket->description = 'Delete launch market';
        $auth->add($deleteLaunchMarket);
        // }}} 

        // Central Launch Manager
        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $auth->addChild($author, $manageLaunch);
        $auth->addChild($author, $createLaunch);
        $auth->addChild($author, $updateLaunch);
        $auth->addChild($author, $deleteLaunch);

        $auth->addChild($mlc, $manageLaunchMarket);
        $auth->addChild($author, $createLaunchMarket);
        $auth->addChild($mlc, $updateLaunchMarket);
        $auth->addChild($author, $deleteLaunchMarket);
        
        $auth->addChild($author, $mlc);
        
        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $author);

        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        $auth->assign($admin, 1);
    }

    public function down()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }
}
