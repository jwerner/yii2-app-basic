<?php

namespace app\components;

use Yii;
use \yii\grid\CheckboxColumn as BaseCheckboxColumn;

/**
 * CheckboxColumn displays a column of checkboxes in a grid view.
 *
 * To add a CheckboxColumn to the [[GridView]], add it to the [[GridView::columns|columns]] configuration as follows:
 *
 * ```php
 * 'columns' => [
 *     // ...
 *     [
 *         'class' => 'yii\grid\CheckboxColumn',
 *         // you may configure additional properties here
 *     ],
 * ]
 * ```
 *
 * Users may click on the checkboxes to select rows of the grid. The selected rows may be
 * obtained by calling the following JavaScript code:
 *
 * ```javascript
 * var keys = $('#grid').yiiGridView('getSelectedRows');
 * // keys is an array consisting of the keys associated with the selected rows
 * ```
 *
 * For more details and usage information on CheckboxColumn, see the [guide article on data widgets](guide:output-data-widgets).
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Joachim Werner <joachim.werner@diggin-data.de> 
 * @since 2.0
 */
class CheckboxColumn extends BaseCheckboxColumn
{
    public $paginationUrl = 'index';
    public $paginationButtonLabel = 'Page Size';
    public $paginationPageSizeParam = 'per-page';
    public $paginationButtonOptions;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if(is_null($this->paginationButtonOptions))
            $this->paginationButtonOptions = ['class'=>'btn btn-primary btn-xs'];
    }

    /**
     * Renders the filter cell content.
     * The default implementation simply renders a space.
     * This method may be overridden to customize the rendering of the filter cell (if any).
     * @return string the rendering result
     */
    protected function renderFilterCellContent()
    {
        return \yii\bootstrap\ButtonGroup::widget([
            'encodeLabels'=>false,
            'buttons' => [ // {{{
                \yii\bootstrap\ButtonDropdown::widget([
                    'label' => $this->paginationButtonLabel,
                    'options'=>$this->paginationButtonOptions,
                    'dropdown' => [
                        'items' => [
                            ['label' => '10',   'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>10]],
                            ['label' => '20',   'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>20]],
                            ['label' => '50',   'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>50]],
                            ['label' => '100',  'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>100]],
                            ['label' => '500',  'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>500]],
                            ['label' => 'All',  'url' => [$this->paginationUrl, $this->paginationPageSizeParam=>0]],
                        ],
                    ],
                ]),
            ],
        ]);
    }
}

