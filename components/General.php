<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\console\Application;

class General extends Component
{
    /**
     * Create and return an application instance
     * if the console app doesn't exist we create a new instance
     * otherwise, it returns the existing instance
     *
     * @return null|\yii\base\Module|Application
     */
    public static function switchToConsoleApp()
    {
        $config = self::getConsoleConfig();
        if (!$consoleApp = Yii::$app->getModule($config['id']))
            $consoleApp = new Application($config);

        // assuming that switching apps only happens when making console commands through the web app,
        // we have to redefine STDOUT to trick the console output into going straight to the web output
        if(!defined('STDOUT'))
            define('STDOUT', fopen('php://output', 'w'));

        return $consoleApp;
    }

    public static function runConsoleActionFromWebApp($action, $params = [])
    {
        $webApp = Yii::$app;
        $consoleApp = self::switchToConsoleApp();
        $result = ($consoleApp->runAction($action, $params) == \yii\console\Controller::EXIT_CODE_NORMAL);
        Yii::$app = $webApp;
        return $result;
    }


    public static function getConsoleConfig()
    {
        return ArrayHelper::merge(
            require(Yii::getAlias('@app/config/console.php')),
            array()
        );
    }
}
