<?php

$db     = array_merge(
    require(__DIR__ . '/db.php'),
    require(__DIR__ . '/db-local.php')
);
$params = array_merge(
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$config = [
    'id' => 'basic-console',
    'name' => 'Yii2 App Basic DD',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'params' => $params,
    'components' => [
        'db' => $db,
        'authManager' => [ // {{{ 
            'class' => 'dektrium\rbac\components\DbManager',
        ], // }}} 
        'cache' => [ // {{{ 
            'class' => 'yii\caching\FileCache',
        ], // }}} 
        'log' => [ // {{{ 
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ], // }}} 
        'mailer' => [ // {{{ 
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ], // }}}  
        'settings' => [ // {{{ 
            'class' => 'yii2mod\settings\components\Settings',
        ], // }}} 
    ],
    'controllerMap' => [
        /*
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
        */
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'nemmo\attachments\migrations',
            ],
            'migrationPath' => [
                '@yii/rbac/migrations',
                '@vendor/dektrium/yii2-user/migrations',
                '@vendor/yii2mod/yii2-settings/migrations',
                '@app/migrations',
                '@app/modules/lookup/migrations',
            ],
        ],
        'migration' => [
            'class' => 'bizley\migration\controllers\MigrationController',
        ],
    ],
    'modules' => [ // {{{ 
        'attachments' => [ // {{{ 
            'class' => nemmo\attachments\Module::className(),
            'tempPath' => '@app/data/uploads/temp',
            'storePath' => '@app/data/uploads/store',
            'rules' => [ // Rules according to the FileValidator
                'maxFiles' => 10, // Allow to upload maximum 3 files, default to 3
                //'mimeTypes' => 'image/png', // Only png images
                'maxSize' => 1024 * 1024 // 1 MB
            ],
            'tableName' => '{{%attachments}}' // Optional, default to 'attach_file'
        ], // }}} 
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
        'user' => [
            'class' => 'dektrium\user\Module',
        ],
    ], // }}} 
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
