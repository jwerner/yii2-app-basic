# Installation of the Yii2 Basic Application Template


## Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
composer create-project diggindata/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~

## Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


## Install with Docker

Clone this repository

Run the installation triggers (creating cookie validation code)

    composer install    
    
Start the containers

    docker-compose up -d
    
You can then access the application through the following URL:

    http://127.0.0.1:8080

You can access Adminer to administer the database through this URL:

    http://127.0.0.1:8081/?server=mariadb&username=root&db=yii2-app-basic

**NOTES:** 
- Minimum required Docker engine version `17.04` for development (see [Performance tuning for volume mounts](https://docs.docker.com/docker-for-mac/osxfs-caching/))
- The default configuration uses a host-volume in your home directory `.docker-composer` for composer caches


# CONFIGURATION

## Configure 

* Create a new MySQL/MariaDB database
* Create `config/db-local.php`
* Create `config/params-local.php`
* Create `config/web-local.php`

...by copying thge provided `config/.sample` files each.


**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


## Run Database Migrations

Either start a shell:

   cd app

Or, start a shell via docker:

    docker-compose exec php-fpm bash

* Run all the following database migrations to install the required tables:

    php yii migrate --migrationPath=@yii/rbac/migrations


* Run the app settings creation:

    yii init-app

Up to now, your database and application is ready to run.

# First Steps

Open your app in the browser:

   http://localhost/yii2-app-basic/web

Click on the **Sign-Up** link and register yourself. After having clicked the **[Sign up]** button, an email file should have been created under _runtime/mail_.

Open the email and click on the _Complete..._ link. You should now be able to log into the application.

Edit the file _config/params-local.php_ and add your username to the _userAdmins_ section. You should now have the menu item Settings > Users Admin.
